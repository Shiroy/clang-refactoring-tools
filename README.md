#clang-refactoring-tools

This repository is a set of refactoring tools built on the top of libclang and libtooling.

The planned tools are:

* A pimplelizer
* A symbol renamer
* A function displacement tool
* A C to C++ cast style converter

All of these tools use a compilation database to configure themselves.